from conan import ConanFile
from conan.tools.files import download
from os import chmod
import stat

class LinuxDeployQt(ConanFile):
    name="linuxdeployqt"
    version="10"
    description="Makes Linux applications self-contained by copying in the libraries and plugins that the application uses, and optionally generates an AppImage. Can be used for Qt and other applications"
    url = "https://github.com/Tereius/conan-linuxdeployqt"
    homepage = "https://github.com/probonopd/linuxdeployqt"
    license = "GPLv3"
    no_copy_source = True
    settings={"os":["Linux"], "arch":["x86_64"]}

    def source(self):
        source_url = "https://github.com/probonopd/linuxdeployqt/releases/download/10/linuxdeployqt-continuous-x86_64.AppImage"
        download(self, source_url, "linuxdeployqt", sha256='62053000cb0544eefcc7cde93e97ba201f1e9a08ef4d888794b10c5299db6595')

    def package(self):
        self.copy(pattern="*")

    def package_id(self):
        self.info.include_build_settings()
        
    def package(self):
        self.copy("*")
        chmod(self.package_folder+"/linuxdeployqt",stat.S_IRWXU)

    def package_info(self):
        self.output.info(f"Adding linuxdeployqt to PATH: {self.package_folder}")
        self.env_info.path.append(self.package_folder)

